#!/bin/bash

# Mount any folders we might require
if [ "x$UNIONFS_MOUNTPOINT" != "x" -a "x$UNIONFS_SOURCE" != "x" ]; then
    if [ ! -d $UNIONFS_MOUNTPOINT ]; then
        mkdir $UNIONFS_MOUNTPOINT
    fi
    echo "Mounting $UNIONFS_SOURCE into $UNIONFS_MOUNTPOINT"
    unionfs-fuse $UNIONFS_OPTIONS $UNIONFS_SOURCE $UNIONFS_MOUNTPOINT || exit
fi

# Start the php-cgi stuff
php-fpm7.3

# Start nginx
nginx

/usr/bin/nfsd.sh
