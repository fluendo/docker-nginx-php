FROM nginx:latest
MAINTAINER fluendo

RUN apt-get update -y && \
    apt-get upgrade -y

RUN mkdir /run/php && \
    touch /run/php/php7.3-fpm.sock


RUN apt-get -y install php7.3-fpm php7.3-xml unionfs-fuse

RUN apt update
RUN apt-get install -y rpcbind nfs-kernel-server

COPY www.conf /etc/php/7.3/fpm/pool.d/
RUN  rm /etc/php/7.3/mods-available/opcache.ini
RUN  rm /etc/php/7.3/cli/conf.d/10-opcache.ini
RUN  rm /etc/php/7.3/fpm/conf.d/10-opcache.ini

COPY nfsd.sh /usr/bin/nfsd.sh
COPY nginx.conf /etc/nginx/nginx.conf


EXPOSE 2049
EXPOSE 80

COPY run.sh .
CMD ["./run.sh"]
